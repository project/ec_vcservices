README.txt
==========

Ecommerce payment module for Virtual Card Services (vcs.co.za) a ZAR credit card 
payment service.

FEATURES
========
Utilises the vcs FETCH & POST method for confirming payments.
Allows for callbacks from vcs to update payment statuses.
Statuses are changed as follows;
canceled - if user cancels payment, 
failed - if payment service declined the credit card, 
payment received - if payment was successful and 
denied - if the PAM (Personal Authentication Message) failed which is a security
feature provided by vcs.
The ec_credit_card table is also filled although this does not seem to be 
implimented on ecommerce at the time this module was published.
The ec_credit_card table contains the reference numbers and reason messages.
Watchdog logs are kept for auditing purposes including the txnid and reference 
for cross checking with vcs.
The module also allows for cel phone number and message text to be included 
for payment confirmation by vcs.
Response pages are customisable.

AUTHOR/MAINTAINER
=================
- nickl
nick AT jigsoft DOT co DOT za

MODULE SPONSOR
==============
http://www.econsultant.co.za

NOTES
=====
Obtain Terminal ID from www.vcs.co.za.
On the vcs account admin site in section Merchant Administration subsection
VCS Interfacing the following configuration needs to be completed.
Http Method needs to be set to FETCH & POST.
The setting for Approved page URL and Declined page URL can be found in the 
administration page for this module as it is specific to your site. 
The Merchant PAM can be configured for a security check.

In the module administration check what the callback URL is for setting on vcs.
Add the VCS Terminal ID as obtained from vcs.
Duplicate the PAM you have configure on vcs. (optional)

